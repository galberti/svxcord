# SvxCord

This toy project is a Discord bot, used to link a Discord audio channel to 
a svxlink reflector. Currently it supports only one talkgroup, due to the
difficulties of selecting different talkgroups via Discord.
The simple code joins [a very good discord
library](https://github.com/bwmarrin/discordgo) and my toy svxlink go library.

## Installation
This assumes you already have a working Go environment, if not please see 
[this page first](https://golang.org/doc/install).
The project uses, among other things, libopus (in debian-like systems:
`apt-get install libopus-dev`).

To pull the latest version and install it:
```
go get gitlab.com/galberti/svxcord
```

## Usage
The command line requires several parameters:

```
  -c string
        channel id
  -s string
        server id (guild)
  -t string
        discord token
  -u string
        bot's status update
  -x string
        svxlink server:port:user:passw:tg
```

All the options are mandatory, except where expressly stated:
 * *channel id* is the discord channel id. It's a rather long number.
 * *server id* is the discord server id, or guild. It's also a long number.
 * *discord token* is the auth token used to authorise on Discord.
 * *status update* is the status of the bot (optional)
 * *svxlink connection parameters* svxlink connect string
 (ex. myhost.com:5300:myuser:mypassword:222)

If you own a Discord server, follow the
[official instructions](https://discordpy.readthedocs.io/en/latest/discord.html)
and related documents to get started with a bot.
The bitrate in the audio channel should be set to 16Kbps, as in svxlink.
Other bitrates are at the moment unsupported.
It's also advised to use an Opus frame size of 10ms in svxlink. Frames sizes
up to 20ms *should* be supported.

It's advised to edit the file `svxcord.sh` and use that to start the program.
It makes use of screen to allow the program to run in background (in
debian-like systems: `apt-get install screen`).

