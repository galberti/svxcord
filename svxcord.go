
package main

import (
    "flag"
    "fmt"
    "os"
    "strings"
    "syscall"
    "time"
    "path/filepath"
    "os/exec"
    "os/signal"

    "github.com/bwmarrin/discordgo"
    "gitlab.com/galberti/gopus"
    "gitlab.com/galberti/saoud/svxlink"
)

const OPUS_BITRATE = 48000

var svxrxc_d chan []byte
var svxtxc_p chan *discordgo.Packet
var svxtxc chan []byte
var vc *discordgo.VoiceConnection
var opusenc *gopus.Encoder
var opusdec *gopus.Decoder

var guildid *string
var chanid *string
var ctrlchanid *string
var scriptsdir *string
var stupdate *string

func ready(s *discordgo.Session, event *discordgo.Ready) {
    // Set the playing status.
    if *stupdate != "" {
        s.UpdateGameStatus(0, *stupdate)
    }
    var err error
    if *chanid == "" {
        return
    }
    vc, err = s.ChannelVoiceJoin(*guildid, *chanid, false, false)
    if err != nil {
        fmt.Printf(err.Error())
        return
    }
    fmt.Fprintf(os.Stderr, "voice channel joined\n")
    svxtxc_p = vc.OpusRecv
    svxrxc_d = vc.OpusSend
}

func command(cmd []string) string {
    switch cmd[0] {
        case "!script":
            sn := filepath.Base(cmd[1])
            cmd := exec.Command(*scriptsdir + "/" + sn, cmd[2:]...)
            out, err := cmd.CombinedOutput()
            if err != nil {
                return fmt.Sprintf("Error:\n%s", err)
            }
            return fmt.Sprintf("Out:\n%s", string(out))
        default:
            return "Command unknown"
    }
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
    // no ctrl chan
    if *ctrlchanid == "" {
        return
    }
    // Ignore all messages created by the bot itself
    if m.Author.ID == s.State.User.ID {
        return
    }
    // ignore messages not from ctrl chan
    if m.ChannelID != *ctrlchanid {
        return
    }
    fmt.Printf("message from %s\n", m.Author.ID)

    //c, err := s.State.Channel(m.ChannelID)
    //if err != nil {
    //    // Could not find channel.
    //    return
    //}
    pcmd := strings.Split(m.Content, " ")
    if len(pcmd) > 0 && strings.HasPrefix(pcmd[0], "!") {
        s.ChannelMessageSend(m.ChannelID, command(pcmd))
    }

//     if strings.HasPrefix(m.Content, "!airhorn") {
// 
//         // Find the channel that the message came from.
//         fmt.Printf("%v\n", m.ChannelID)
// 
//         // Find the guild for that channel.
//         g, err := s.State.Guild(c.GuildID)
//         if err != nil {
//             // Could not find guild.
//             return
//         }
//         fmt.Printf("%v\n", g)
// 
//         // Look for the message sender in that guild's current voice states.
//         for _, vs := range g.VoiceStates {
//             if vs.UserID == m.Author.ID {
//                 err = playSound(s, g.ID, vs.ChannelID)
//                 if err != nil {
//                     fmt.Println("Error playing sound:", err)
//                 }
// 
//                 return
//             }
//         }
//      }
}

func discord_to_svx(){
    for {
        p := <-svxtxc_p
        //fmt.Printf("%d\n", p.Timestamp)
        //fmt.Printf("    ty: %04x\n", p.Type)
        //fmt.Printf("    sz: %d\n", len(p.Opus))
        //fmt.Printf("    ch: %d\n", gopus.OpusPacketGetNbChannels(p.Opus))
        //fmt.Printf("    bw: %d\n", gopus.OpusPacketGetBandwidth(p.Opus))
        svxtxc <- p.Opus
    }
}

func svx_to_discord(input, output chan []byte) {
    for {
        bin1 := <-input
        sdec1, _ := opusdec.Decode(bin1, OPUS_BITRATE/10, false)
        d := len(sdec1)
        if d == 960 {
            output <-bin1
            continue
        }
        if d > 960 {
            fmt.Fprintf(os.Stderr, "OPUS > 20ms unsupported\n")
            continue
        }
        sdecs := make([]int16, 960)
        copy(sdecs, sdec1)
        for i := d; (i + d) <= 960; i+=d {
            // discordgo wants 20ms (960 samples)
            bin2 := <-input
            sdec2, _ := opusdec.Decode(bin2, OPUS_BITRATE/10, false)
            copy(sdecs[i:], sdec2)
        }
        end, _ := opusenc.Encode(sdecs, len(sdecs), 1276)
        output <- end
    }
}

func main() {
    // mandatory
    token := flag.String("t", "", "discord token")
    guildid = flag.String("s", "", "server id (guild)")

    // either both or none
    svxd := flag.String("x", "",
        "svxlink server:port:user:passw:tg (needed with -c)")
    chanid = flag.String("c", "", "channel id (needed with -x)")

    // either both or none
    ctrlchanid = flag.String("k", "", "control channel id (needed with -e)")
    scriptsdir = flag.String("e", "", "scripts directory (needed with -k)")

    // optional
    stupdate = flag.String("u", "", "bot's status update")
    flag.Parse()

    if *token == "" || *guildid == "" {
        fmt.Fprintf(os.Stderr, "-t and -s are both needed\n")
        flag.PrintDefaults()
        return
    }

    if (*svxd != "" && *chanid == "") || (*svxd == "" && *chanid != "") {
        fmt.Fprintf(os.Stderr, "-x and -c are either both or none needed\n")
        flag.PrintDefaults()
        return
    }

    if (*scriptsdir != "" && *ctrlchanid == "") ||
        (*scriptsdir == "" && *ctrlchanid != "") {
        fmt.Fprintf(os.Stderr, "-k and -e are either both or none needed.\n")
        flag.PrintDefaults()
        return
    }

    dg, err := discordgo.New("Bot " + *token)
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error creating Discord session: %s\n",
            err.Error())
        return
    }

    // Register ready as a callback for the ready events.
    dg.AddHandler(ready)
    dg.AddHandler(messageCreate)
    // Open the websocket and begin listening.
    err = dg.Open()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error opening Discord session: %s\n",
            err.Error())
    }

    if *chanid != "" {
        // wait for discord to be ready
        for {
            if svxrxc_d != nil {
                break
            }
            time.Sleep(100 * time.Millisecond)
        }

        fmt.Fprintf(os.Stderr, "prepare Opus\n")
        opusdec, _ = gopus.NewDecoder(OPUS_BITRATE, 1)
        opusenc, _ = gopus.NewEncoder(OPUS_BITRATE, 1, gopus.Voip)
        opusenc.SetBitrate(16000)

        fmt.Fprintf(os.Stderr, "prepare rx/tx goroutines\n")
        svxrxc := make(chan []byte)
        svxtxc = make(chan []byte)
        go svx_to_discord(svxrxc, svxrxc_d)
        go discord_to_svx()

        fmt.Fprintf(os.Stderr, "prepare svxlink\n")
        s := svxlink.SVXLink{}
        pars := strings.Split(*svxd, ":")
        svx := s.Init(pars[0] + ":" + pars[1], pars[2], pars[3], pars[4],
            svxrxc, svxtxc)
        if !svx {
            fmt.Fprintf(os.Stderr, "Error creating svxlink session\n")
            return
        }
        // I am always speaking .. 
        vc.Speaking(true)
    }

    // hang
    sc := make(chan os.Signal, 1)
    signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
    <-sc

    // Cleanly close down the Discord session.
    dg.Close()
}

